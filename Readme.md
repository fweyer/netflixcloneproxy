### NetflixProxy

This project is part of a 3 tier architecture and serves as a proxy between a frontend application and the API  theMovieDB. The task is to return movies by categories. In addition, it regulates the authentication and authorization for new users.

#### Build

This Project can be build by the command "mvn clean install" to run this Project you need
an API Key from the MovieDB and an application.yaml. The key and the base-ugitrl must be entered in the application.yaml.