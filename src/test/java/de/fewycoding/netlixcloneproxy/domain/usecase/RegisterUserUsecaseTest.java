package de.fewycoding.netlixcloneproxy.domain.usecase;

import de.fewycoding.netlixcloneproxy.api.request.RegisterUser;
import de.fewycoding.netlixcloneproxy.database.entity.RoleEntity;
import de.fewycoding.netlixcloneproxy.database.entity.UserEntity;
import de.fewycoding.netlixcloneproxy.database.repository.RoleRepository;
import de.fewycoding.netlixcloneproxy.database.repository.UserRepository;
import de.fewycoding.netlixcloneproxy.exception.EmptyResultException;
import de.fewycoding.netlixcloneproxy.exception.RegisterUserCaseException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;

class RegisterUserUsecaseTest {


    @Mock
    UserRepository userRepository;

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    RoleRepository roleRepository;

    @InjectMocks
    RegisterUserUsecase registerUserUsecase;

    @BeforeEach
    void setup(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void userSuccesfulCreated() {
        UserEntity userEntity = new UserEntity();
        userEntity.setName("max");
        userEntity.setEmail("max@max.de");
        userEntity.setPassword("123456");
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setName("ROLE_USER");
        userEntity.getRoles().add(roleEntity);
        Mockito.when(userRepository.findByEmail("max@max.de")).thenReturn(null);
        Mockito.when(passwordEncoder.encode("123456")).thenReturn("123456");
        Mockito.when(roleRepository.findByName("ROLE_USER")).thenReturn(roleEntity);

        RegisterUser registerUser = new RegisterUser("max", "max@max.de", "123456" );

        registerUserUsecase.execute(registerUser);

        Mockito.verify(userRepository).save(userEntity);
    }

    @Test
    void userAllwaysRegisterd() {
        UserEntity userEntity = new UserEntity();
        userEntity.setName("max");
        userEntity.setEmail("max@max.de");
        userEntity.setPassword("123456");
        RoleEntity roleEntity = new RoleEntity();
        roleEntity.setName("ROLE_USER");
        userEntity.getRoles().add(roleEntity);
        Mockito.when(userRepository.findByEmail("max@max.de")).thenReturn(userEntity);

        RegisterUser registerUser = new RegisterUser("max", "max@max.de", "123456" );
        assertThrows(RegisterUserCaseException.class, () -> registerUserUsecase.execute(registerUser));
    }
}