package de.fewycoding.netlixcloneproxy.domain.service;

import de.fewycoding.netlixcloneproxy.config.MovieURLConfig;
import de.fewycoding.netlixcloneproxy.domain.dto.MovieDto;
import de.fewycoding.netlixcloneproxy.domain.dto.MovieListResultDto;
import de.fewycoding.netlixcloneproxy.domain.service.impl.MovieServiceImpl;
import de.fewycoding.netlixcloneproxy.exception.EmptyResultException;
import de.fewycoding.netlixcloneproxy.restclient.MovieDBClient;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MovieServiceTest {

    @Mock
    MovieDBClient movieDBClient;

    @Mock
    MovieURLConfig urlConfig;

    @InjectMocks
    MovieServiceImpl movieService;

    @BeforeEach
    void setup(){
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void fetchTrending_getData() {
        MovieDto movieDto = new MovieDto();
        movieDto.setName("Spiderman");
        List<MovieDto> movieResults = List.of(movieDto);
        MovieListResultDto movieListResultDto = new MovieListResultDto();
        movieListResultDto.setResults(movieResults);
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getTrending())).thenReturn(movieListResultDto);

        MovieListResultDto actual = movieService.fetchTrending();

        assertNotNull(actual);
        assertEquals(actual.getResults().get(0).getName(), "Spiderman");
    }

    @Test
    void fetchNetflixOriginals_getData() {
        MovieDto movieDto = new MovieDto();
        movieDto.setName("huibuh");
        List<MovieDto> movieResults = List.of(movieDto);
        MovieListResultDto movieListResultDto = new MovieListResultDto();
        movieListResultDto.setResults(movieResults);
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getNetflixOriginals())).thenReturn(movieListResultDto);

        MovieListResultDto actual = movieService.fetchNetflixOriginals();

        assertNotNull(actual);
        assertEquals(actual.getResults().get(0).getName(), "huibuh");
    }

    @Test
    void fetchTopRated_getData() {
        MovieDto movieDto = new MovieDto();
        movieDto.setName("squidgame");
        List<MovieDto> movieResults = List.of(movieDto);
        MovieListResultDto movieListResultDto = new MovieListResultDto();
        movieListResultDto.setResults(movieResults);
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getTopRated())).thenReturn(movieListResultDto);

        MovieListResultDto actual = movieService.fetchTopRated();

        assertNotNull(actual);
        assertEquals(actual.getResults().get(0).getName(), "squidgame");
    }

    @Test
    void fetchActionMovies_getData() {
        MovieDto movieDto = new MovieDto();
        movieDto.setName("Terminator");
        List<MovieDto> movieResults = List.of(movieDto);
        MovieListResultDto movieListResultDto = new MovieListResultDto();
        movieListResultDto.setResults(movieResults);
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getAction())).thenReturn(movieListResultDto);

        MovieListResultDto actual = movieService.fetchActionMovies();

        assertNotNull(actual);
        assertEquals(actual.getResults().get(0).getName(), "Terminator");
    }

    @Test
    void fetchComedyMovies_getData() {
        MovieDto movieDto = new MovieDto();
        movieDto.setName("Hangover");
        List<MovieDto> movieResults = List.of(movieDto);
        MovieListResultDto movieListResultDto = new MovieListResultDto();
        movieListResultDto.setResults(movieResults);
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getComedy())).thenReturn(movieListResultDto);

        MovieListResultDto actual = movieService.fetchComedyMovies();

        assertNotNull(actual);
        assertEquals(actual.getResults().get(0).getName(), "Hangover");
    }

    @Test
    void fetchHorrorMovies_getData() {
        MovieDto movieDto = new MovieDto();
        movieDto.setName("Hanibal");
        List<MovieDto> movieResults = List.of(movieDto);
        MovieListResultDto movieListResultDto = new MovieListResultDto();
        movieListResultDto.setResults(movieResults);
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getHorror())).thenReturn(movieListResultDto);

        MovieListResultDto actual = movieService.fetchHorrorMovies();

        assertNotNull(actual);
        assertEquals(actual.getResults().get(0).getName(), "Hanibal");
    }

    @Test
    void fetchRomanceMovies_getData() {
        MovieDto movieDto = new MovieDto();
        movieDto.setName("Dirtydancing");
        List<MovieDto> movieResults = List.of(movieDto);
        MovieListResultDto movieListResultDto = new MovieListResultDto();
        movieListResultDto.setResults(movieResults);
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getRomance())).thenReturn(movieListResultDto);

        MovieListResultDto actual = movieService.fetchRomanceMovies();

        assertNotNull(actual);
        assertEquals(actual.getResults().get(0).getName(), "Dirtydancing");
    }

    @Test()
    void fetchTrending_getReponseNull() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getTrending())).thenReturn(null);
        assertThrows(EmptyResultException.class, () -> movieService.fetchTrending());
    }

    @Test()
    void fetchNetflixOriginals_getReponseNull() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getNetflixOriginals())).thenReturn(null);
        assertThrows(EmptyResultException.class, () -> movieService.fetchNetflixOriginals());
    }

    @Test()
    void fetchTopRated_getReponseNull() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getTopRated())).thenReturn(null);
        assertThrows(EmptyResultException.class, () -> movieService.fetchTopRated());
    }

    @Test()
    void fetchActionMovies_getReponseNull() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getAction())).thenReturn(null);
        assertThrows(EmptyResultException.class, () -> movieService.fetchActionMovies());
    }

    @Test()
    void fetchComedyMovies_getReponseNull() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getComedy())).thenReturn(null);
        assertThrows(EmptyResultException.class, () -> movieService.fetchComedyMovies());
    }

    @Test()
    void fetchHorrorMovies_getReponseNull() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getHorror())).thenReturn(null);
        assertThrows(EmptyResultException.class, () -> movieService.fetchHorrorMovies());
    }

    @Test()
    void fetchRomanceMovies_getReponseNull() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getRomance())).thenReturn(null);
        assertThrows(EmptyResultException.class, () -> movieService.fetchRomanceMovies());
    }
    @Test()
    void fetchTrending_getReponseEmpty() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getTrending())).thenReturn(new MovieListResultDto());
        assertThrows(EmptyResultException.class, () -> movieService.fetchTrending());
    }

    @Test()
    void fetchNetflixOriginals_getReponseEmpty() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getNetflixOriginals())).thenReturn(new MovieListResultDto());
        assertThrows(EmptyResultException.class, () -> movieService.fetchNetflixOriginals());
    }

    @Test()
    void fetchTopRated_getReponseEmpty() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getTopRated())).thenReturn(new MovieListResultDto());
        assertThrows(EmptyResultException.class, () -> movieService.fetchTopRated());
    }

    @Test()
    void fetchActionMovies_getReponseEmpty() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getAction())).thenReturn(new MovieListResultDto());
        assertThrows(EmptyResultException.class, () -> movieService.fetchActionMovies());
    }

    @Test()
    void fetchComedyMovies_getReponseEmpty() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getComedy())).thenReturn(new MovieListResultDto());
        assertThrows(EmptyResultException.class, () -> movieService.fetchComedyMovies());
    }

    @Test()
    void fetchHorrorMovies_getReponseEmpty() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getHorror())).thenReturn(new MovieListResultDto());
        assertThrows(EmptyResultException.class, () -> movieService.fetchHorrorMovies());
    }

    @Test()
    void fetchRomanceMovies_getReponseEmpty() {
        Mockito.when(movieDBClient.fetchMovies(urlConfig.getRomance())).thenReturn(new MovieListResultDto());
        assertThrows(EmptyResultException.class, () -> movieService.fetchRomanceMovies());
    }
}