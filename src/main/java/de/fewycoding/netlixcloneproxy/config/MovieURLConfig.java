package de.fewycoding.netlixcloneproxy.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
@NoArgsConstructor
public class MovieURLConfig {
    @Value("${moviedb.requests.fetchTrending}")
    private String trending;
    @Value("${moviedb.requests.fetchNetflixOriginals}")
    private String netflixOriginals;
    @Value("${moviedb.requests.fetchTopRated}")
    private String topRated;
    @Value("${moviedb.requests.fetchActionMovies}")
    private String action;
    @Value("${moviedb.requests.fetchComedyMovies}")
    private String comedy;
    @Value("${moviedb.requests.fetchHorrorMovies}")
    private String horror;
    @Value("${moviedb.requests.fetchRomanceMovies}")
    private String romance;
}
