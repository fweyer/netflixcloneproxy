package de.fewycoding.netlixcloneproxy.config;

import de.fewycoding.netlixcloneproxy.domain.dto.RoleDto;
import de.fewycoding.netlixcloneproxy.domain.dto.UserDto;
import de.fewycoding.netlixcloneproxy.domain.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

@Configuration
public class WebConfig {

    @Bean
    PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }

    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    CommandLineRunner run(UserService userService){
        return args -> {
            userService.saveRole(new RoleDto("ROLE_USER"));
            userService.saveRole(new RoleDto("ROLE_ADMIN"));

            userService.saveUser(new UserDto("Max", "max@max.de","123456",new ArrayList<>()));
            userService.saveUser(new UserDto("Moritz", "moritz@moritz.de","123456",new ArrayList<>()));

            userService.addRoleToUser("max@max.de", "ROLE_USER");
            userService.addRoleToUser("moritz@moritz.de", "ROLE_USER");
            userService.addRoleToUser("moritz@moritz.de", "ROLE_ADMIN");
        };
    }
}
