package de.fewycoding.netlixcloneproxy.util.mapper;

import de.fewycoding.netlixcloneproxy.database.entity.RoleEntity;
import de.fewycoding.netlixcloneproxy.domain.dto.RoleDto;

public class RoleMapper {

    public static RoleEntity mapToEntity(RoleDto roleDto) {
        RoleEntity entity = new RoleEntity();
        entity.setName(roleDto.getName());
        return entity;
    }

    public static RoleDto mapToDto(RoleEntity entity) {
        return new RoleDto(
                entity.getName()
        );
    }
}
