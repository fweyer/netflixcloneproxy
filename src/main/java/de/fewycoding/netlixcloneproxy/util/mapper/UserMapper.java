package de.fewycoding.netlixcloneproxy.util.mapper;

import de.fewycoding.netlixcloneproxy.database.entity.RoleEntity;
import de.fewycoding.netlixcloneproxy.database.entity.UserEntity;
import de.fewycoding.netlixcloneproxy.domain.dto.RoleDto;
import de.fewycoding.netlixcloneproxy.domain.dto.UserDto;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class UserMapper {

    public static UserEntity mapToEntity(UserDto userDto) {
        UserEntity entity = new UserEntity();
        entity.setEmail(userDto.getEmail());
        entity.setName(userDto.getName());
        entity.setPassword(userDto.getPassword());
        List<RoleEntity> roleEntities = mapToRoleEntityList(userDto.getRoles());
        entity.setRoles(roleEntities);
        return entity;
    }

    public static UserDto mapToDto(UserEntity entity) {
        return new UserDto(
                entity.getName(),
                entity.getEmail(),
                entity.getPassword(),
                mapToRoleDtoList(entity.getRoles())
        );
    }

    private static List<RoleDto> mapToRoleDtoList(Collection<RoleEntity> roleEntityList){
        return roleEntityList.stream()
                .map(RoleMapper::mapToDto)
                .collect(Collectors.toList());
    }

    private static List<RoleEntity> mapToRoleEntityList(Collection<RoleDto> roleDtoList){
        return roleDtoList.stream()
                .map(RoleMapper::mapToEntity)
                .collect(Collectors.toList());
    }
}
