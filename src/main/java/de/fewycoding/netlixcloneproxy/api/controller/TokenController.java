package de.fewycoding.netlixcloneproxy.api.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.fewycoding.netlixcloneproxy.domain.dto.RoleDto;
import de.fewycoding.netlixcloneproxy.domain.dto.UserDto;
import de.fewycoding.netlixcloneproxy.domain.service.UserService;
import de.fewycoding.netlixcloneproxy.security.filter.CustomAuthenticationFilter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/token")
@Slf4j
public class TokenController {
    private final UserService userService;

    @PostMapping("/refresh")
    public void getRefreshToken(HttpServletRequest request, HttpServletResponse response, @RequestBody String token) throws IOException {
        log.info("Refresh " + token);
        if (token != null){
            try {
                String refreshToken = token;
                Algorithm algorithm = Algorithm.HMAC256("secret".getBytes()); // Secret muss aus einer Property file kommen UUID oder sowas
                JWTVerifier verifier = JWT.require(algorithm).build();
                DecodedJWT decodedJWT = verifier.verify(refreshToken);
                String email = decodedJWT.getSubject();
                UserDto userDto = userService.getUser(email);
                List<String> authorities = userDto.getRoles().stream().map(RoleDto::getName).collect(Collectors.toList());
                String accessToken = CustomAuthenticationFilter.createAccessToken(userDto.getEmail(),authorities,request,algorithm);
                Map<String,String> tokens = new HashMap<>();
                tokens.put("access_token", accessToken);
                tokens.put("refresh_token", refreshToken);
                response.setContentType(APPLICATION_JSON_VALUE);
                new ObjectMapper().writeValue(response.getOutputStream(),tokens);
            }catch (Exception e){
                log.error("Error loggin in: {}", e.getMessage());
                response.setHeader("Error", e.getMessage());
                response.sendError(HttpStatus.FORBIDDEN.value());
            }
        } else {
            throw new RuntimeException("Refreshtoken is missing");
        }
    }
}
