package de.fewycoding.netlixcloneproxy.api.controller;

import de.fewycoding.netlixcloneproxy.api.request.RegisterUser;
import de.fewycoding.netlixcloneproxy.domain.usecase.RegisterUserUsecase;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
@AllArgsConstructor
@Slf4j
public class RegisterController {
    private final RegisterUserUsecase registerUserUsecase;

    @PostMapping("/register")
    public ResponseEntity<String> registerUser(@RequestBody @Validated RegisterUser registerUser){
            registerUserUsecase.execute(registerUser);
            return new ResponseEntity("User succesful created",HttpStatus.NO_CONTENT);
    }
}
