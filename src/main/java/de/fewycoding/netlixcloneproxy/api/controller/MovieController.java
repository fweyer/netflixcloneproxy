package de.fewycoding.netlixcloneproxy.api.controller;

import de.fewycoding.netlixcloneproxy.domain.dto.MovieListResultDto;
import de.fewycoding.netlixcloneproxy.domain.service.MovieService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/movie")
@AllArgsConstructor
public class MovieController {

    private final MovieService movieService;

    @GetMapping("/trending")
    public ResponseEntity<MovieListResultDto> fetchTrending(){
        return ResponseEntity.ok( movieService.fetchTrending());
    }

    @GetMapping("/netflixoriginals")
    public ResponseEntity<MovieListResultDto> fetchNetflixOriginals(){
        return ResponseEntity.ok( movieService.fetchNetflixOriginals());
    }

    @GetMapping("/topradet")
    public ResponseEntity<MovieListResultDto> fetchTopRated(){
        return ResponseEntity.ok( movieService.fetchTopRated());
    }

    @GetMapping("/action")
    public ResponseEntity<MovieListResultDto> fetchActionMovies(){
        return ResponseEntity.ok( movieService.fetchActionMovies());
    }

    @GetMapping("/comedy")
    public ResponseEntity<MovieListResultDto> fetchComedyMovies(){
        return ResponseEntity.ok( movieService.fetchComedyMovies());
    }

    @GetMapping("/horror")
    public ResponseEntity<MovieListResultDto> fetchHorrorMovies(){
        return ResponseEntity.ok( movieService.fetchHorrorMovies());
     }

    @GetMapping("/romance")
    public ResponseEntity<MovieListResultDto> fetchRomanceMovies(){
        return ResponseEntity.ok( movieService.fetchRomanceMovies());
    }
}
