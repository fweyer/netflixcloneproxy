package de.fewycoding.netlixcloneproxy.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND,reason = "There are no movies available")
public class EmptyResultException extends RuntimeException {
    public EmptyResultException(String s) {}
}
