package de.fewycoding.netlixcloneproxy.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.PRECONDITION_FAILED,reason = "Der User ist schon registriert")
public class RegisterUserCaseException extends RuntimeException {
    public RegisterUserCaseException(String s) { }
}
