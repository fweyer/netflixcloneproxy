package de.fewycoding.netlixcloneproxy.domain.service.impl;

import de.fewycoding.netlixcloneproxy.config.MovieURLConfig;
import de.fewycoding.netlixcloneproxy.domain.dto.MovieListResultDto;
import de.fewycoding.netlixcloneproxy.domain.service.MovieService;
import de.fewycoding.netlixcloneproxy.exception.EmptyResultException;
import de.fewycoding.netlixcloneproxy.restclient.MovieDBClient;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class MovieServiceImpl implements MovieService {

    private final MovieDBClient movieDBClient;
    private final MovieURLConfig urlConfig;

    @Override
    public MovieListResultDto fetchTrending()  {
        MovieListResultDto movieListResultDto = movieDBClient.fetchMovies(urlConfig.getTrending());
        if (isMovieResultNullorEmpty(movieListResultDto)){
            throw new EmptyResultException("There are no movies available");
        }
        return movieListResultDto;
    }

    @Override
    public MovieListResultDto fetchNetflixOriginals() {
        MovieListResultDto movieListResultDto = movieDBClient.fetchMovies(urlConfig.getNetflixOriginals());
        if (isMovieResultNullorEmpty(movieListResultDto)){
            throw new EmptyResultException("There are no movies available");
        }
        return movieListResultDto;
    }

    @Override
    public MovieListResultDto fetchTopRated()  {
        MovieListResultDto movieListResultDto = movieDBClient.fetchMovies(urlConfig.getTopRated());
        if (isMovieResultNullorEmpty(movieListResultDto)){
            throw new EmptyResultException("There are no movies available");
        }
        return movieListResultDto;
    }

    @Override
    public MovieListResultDto fetchActionMovies()  {
        MovieListResultDto movieListResultDto = movieDBClient.fetchMovies(urlConfig.getAction());
        if (isMovieResultNullorEmpty(movieListResultDto)){
            throw new EmptyResultException("There are no movies available");
        }
        return movieListResultDto;
    }

    @Override
    public MovieListResultDto fetchComedyMovies()  {
        MovieListResultDto movieListResultDto = movieDBClient.fetchMovies(urlConfig.getComedy());
        if (isMovieResultNullorEmpty(movieListResultDto)){
            throw new EmptyResultException("There are no movies available");
        }
        return movieListResultDto;
    }

    @Override
    public MovieListResultDto fetchHorrorMovies()  {
        MovieListResultDto movieListResultDto = movieDBClient.fetchMovies(urlConfig.getHorror());
        if (isMovieResultNullorEmpty(movieListResultDto)){
            throw new EmptyResultException("There are no movies available");
        }
        return movieListResultDto;
    }

    @Override
    public MovieListResultDto fetchRomanceMovies()  {
        MovieListResultDto movieListResultDto = movieDBClient.fetchMovies(urlConfig.getRomance());
        if (isMovieResultNullorEmpty(movieListResultDto)){
            throw new EmptyResultException("There are no movies available");
        }
        return movieListResultDto;
    }

    private boolean isMovieResultNullorEmpty(MovieListResultDto listResultDto){
        return listResultDto == null ||
               listResultDto.getResults() == null ||
               listResultDto.getResults().isEmpty();
    }
}
