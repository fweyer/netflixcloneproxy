package de.fewycoding.netlixcloneproxy.domain.service;

import de.fewycoding.netlixcloneproxy.domain.dto.RoleDto;
import de.fewycoding.netlixcloneproxy.domain.dto.UserDto;

public interface UserService {
    UserDto saveUser(UserDto user);
    RoleDto saveRole(RoleDto roleDto);
    void addRoleToUser(String email, String rolename);
    UserDto getUser(String email);
}
