package de.fewycoding.netlixcloneproxy.domain.service;

import de.fewycoding.netlixcloneproxy.domain.dto.MovieListResultDto;

public interface MovieService {
    MovieListResultDto fetchTrending();
    MovieListResultDto fetchNetflixOriginals();
    MovieListResultDto fetchTopRated();
    MovieListResultDto fetchActionMovies();
    MovieListResultDto fetchComedyMovies();
    MovieListResultDto fetchHorrorMovies();
    MovieListResultDto fetchRomanceMovies();
}

