package de.fewycoding.netlixcloneproxy.domain.service.impl;

import de.fewycoding.netlixcloneproxy.database.entity.RoleEntity;
import de.fewycoding.netlixcloneproxy.database.entity.UserEntity;
import de.fewycoding.netlixcloneproxy.database.repository.RoleRepository;
import de.fewycoding.netlixcloneproxy.database.repository.UserRepository;
import de.fewycoding.netlixcloneproxy.domain.dto.RoleDto;
import de.fewycoding.netlixcloneproxy.domain.dto.UserDto;
import de.fewycoding.netlixcloneproxy.domain.service.UserService;
import de.fewycoding.netlixcloneproxy.util.mapper.RoleMapper;
import de.fewycoding.netlixcloneproxy.util.mapper.UserMapper;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Collection;

@Service
@AllArgsConstructor
@Transactional
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserEntity userEntity = userRepository.findByEmail(email);
        if (userEntity == null){
            log.error("User not found");
            throw new UsernameNotFoundException("User not found");
        }
        log.debug("User {] found", userEntity.getEmail());
        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        userEntity.getRoles().forEach(roleEntity ->  authorities.add(new SimpleGrantedAuthority(roleEntity.getName())));
        return new User(userEntity.getEmail(),userEntity.getPassword(),authorities);
    }

    @Override
    public UserDto saveUser(UserDto user) {
        UserEntity entity = UserMapper.mapToEntity(user);
        entity.setPassword(passwordEncoder.encode(user.getPassword()));
        log.debug("User {] insert into database", entity.getEmail());
        return UserMapper.mapToDto(userRepository.save(entity));
    }

    @Override
    public RoleDto saveRole(RoleDto roleDto) {
        RoleEntity entity = RoleMapper.mapToEntity(roleDto);
        log.debug("Rolle {} insert into database", entity.getName());
        return RoleMapper.mapToDto(roleRepository.save(entity));
    }

    @Override
    public void addRoleToUser(String email, String rolename) {
        UserEntity userEntity = userRepository.findByEmail(email);
        RoleEntity roleEntity = roleRepository.findByName(rolename);
        userEntity.getRoles().add(roleEntity);
        log.debug("Role {] from User {} inserted", roleEntity.getName() ,userEntity.getEmail());
        userRepository.save(userEntity);
    }

    @Override
    public UserDto getUser(String email) {
        return UserMapper.mapToDto(userRepository.findByEmail(email));
    }

}
