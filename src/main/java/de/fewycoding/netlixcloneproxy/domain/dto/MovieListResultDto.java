package de.fewycoding.netlixcloneproxy.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MovieListResultDto {
    private int page;
    private List<MovieDto> results;
    private int total_results;
    private int total_pages;
}
