package de.fewycoding.netlixcloneproxy.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Collection;

@Data
@AllArgsConstructor
public class UserDto {
    private String name;
    private String email;
    private String password;
    private Collection<RoleDto> roles;
}
