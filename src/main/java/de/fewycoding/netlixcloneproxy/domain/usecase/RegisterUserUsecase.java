package de.fewycoding.netlixcloneproxy.domain.usecase;

import de.fewycoding.netlixcloneproxy.api.request.RegisterUser;
import de.fewycoding.netlixcloneproxy.database.entity.RoleEntity;
import de.fewycoding.netlixcloneproxy.database.entity.UserEntity;
import de.fewycoding.netlixcloneproxy.database.repository.RoleRepository;
import de.fewycoding.netlixcloneproxy.database.repository.UserRepository;
import de.fewycoding.netlixcloneproxy.exception.RegisterUserCaseException;
import de.fewycoding.netlixcloneproxy.util.annotations.UseCase;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;

@UseCase
@RequiredArgsConstructor
@Slf4j
public class RegisterUserUsecase {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    public void execute(RegisterUser registerUser) {
        if (isUserAlreadyRegisterd(registerUser)){
          throw new RegisterUserCaseException("User is already registerd under that email");
        }
        UserEntity userEntity = mapToEntity(registerUser);
        userEntity.setPassword(passwordEncoder.encode(registerUser.getPassword()));
        RoleEntity roleEntity =  roleRepository.findByName("ROLE_USER");
        userEntity.getRoles().add(roleEntity);
        userRepository.save(userEntity);
        log.debug("User {} insert into database", userEntity.getEmail());
    }

    private UserEntity mapToEntity(RegisterUser registerUser){
        UserEntity userEntity = new UserEntity();
        userEntity.setEmail(registerUser.getEmail());
        userEntity.setName(registerUser.getUserName());
        userEntity.setPassword(registerUser.getPassword());
        return  userEntity;
    }

    private boolean isUserAlreadyRegisterd(RegisterUser registerUser){
        return userRepository.findByEmail(registerUser.getEmail()) != null;
    }
}
