package de.fewycoding.netlixcloneproxy.restclient;

import de.fewycoding.netlixcloneproxy.domain.dto.MovieListResultDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class MovieDBClient {

    @Autowired
    private RestTemplate restTemplate;

    public MovieListResultDto fetchMovies(String url){
        return restTemplate.getForObject(url, MovieListResultDto.class);
    }
}
